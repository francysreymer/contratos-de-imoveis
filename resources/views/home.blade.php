<html>
    <body>

        <div id="app">
            <property-component></property-component>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>